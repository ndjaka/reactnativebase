import { RegisterRequest, RegisterSuccess, RegisterFailed, AccountState } from './types/AccountTypes';



export * from './types/AccountTypes';

export interface ApplicationState {
 account:AccountState
}
  
export type ApplicationAction = 
|RegisterRequest
|RegisterFailed
|RegisterSuccess;