import { RegisterRequest, REGISTER_REQUEST, RegisterSuccess, REGISTER_SUCCESS, RegisterFailed, REGISTER_FAILED } from "../types";
import { User } from "../types/UserTypes";

export const registerRequest = (): RegisterRequest => ({
    type:REGISTER_REQUEST

})

export const registerSuccess = (user:User):RegisterSuccess => ({
    user:user,
    type:REGISTER_SUCCESS
})

export const registerFailed = (): RegisterFailed => ({
    type:REGISTER_FAILED
})