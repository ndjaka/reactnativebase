import { ThunkAction } from "redux-thunk";
import { ApplicationState, ApplicationAction } from "../types";
import { User } from "../types/UserTypes";
import { AuthService } from "src/services/api";
import { registerSuccess, registerRequest, registerFailed } from "../actions";

type Effect = ThunkAction<any, ApplicationState, any, ApplicationAction>;


export const registerEffect = (user: User): Effect => async (
    dispatch,
    getState
) => {
    return AuthService.register(user).then(async (res: any) => {
        if ([200, 201].indexOf(res.status) !== -1) {
            let { data} = await res.json();
            dispatch(registerRequest());
            return dispatch(registerSuccess(data))
        }else{
            return  dispatch(registerFailed())
        }
    }).catch((err:any)=>{
        return dispatch(registerFailed());
    })
}