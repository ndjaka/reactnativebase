import { combineReducers, Reducer } from 'redux';
import { ApplicationState } from './types';
import AccountReducer from './reducers/AccountReducer';

const rootReducer: Reducer<ApplicationState> = combineReducers<
  ApplicationState
>({
  account:AccountReducer
});

export default rootReducer;