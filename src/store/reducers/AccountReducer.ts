import { AccountState, REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_FAILED } from "../types/AccountTypes";
import { ApplicationAction } from "../types";
import produce from 'immer';


const initialState: AccountState = {
    loading: {
        register_loading_failed: false,
        register_loading_request: false,
        register_loading_success: false
    },
    user: {
        email: '',
        firstname: '',
        lastname: ''
    }
};

const reducer = (state = initialState, action: ApplicationAction) => {
    switch (action.type) {
        case REGISTER_REQUEST:
            return produce(state, (draft) => {
                draft.loading.register_loading_failed = false;
                draft.loading.register_loading_success = false;
                draft.loading.register_loading_request = true
            });
        case REGISTER_SUCCESS:
            return produce(state, (draft) => {
                draft.loading.register_loading_failed = false;
                draft.loading.register_loading_success = true;
                draft.loading.register_loading_request = true
            });
        case REGISTER_FAILED:
            return produce(state, (draft) => {
                draft.loading.register_loading_failed = true;
                draft.loading.register_loading_success = false;
                draft.loading.register_loading_request = false
            });

        default: {
            return state;
        }

    }
}
export default reducer;