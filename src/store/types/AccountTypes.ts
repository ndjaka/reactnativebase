import { User } from "./UserTypes";
import { Action } from 'redux';


export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_FAILED = 'REGISTER_FAILED';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';

export interface LoadingAccountState {
    register_loading_request:boolean,
    register_loading_success:boolean,
    register_loading_failed:boolean,

}

export interface AccountState {
    user:User;
    loading:LoadingAccountState
}

export interface RegisterRequest extends Action{
    type: typeof REGISTER_REQUEST;
}

export interface RegisterSuccess extends Action{
    type: typeof REGISTER_SUCCESS;
    user:User
}

export interface RegisterFailed extends Action{
    type: typeof REGISTER_FAILED;
}

