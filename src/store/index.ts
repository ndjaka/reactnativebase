import { applyMiddleware, createStore, compose, Store } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'remote-redux-devtools';
import rootReducer from './reducer';
import { ApplicationState } from './types';

const middlewares = [];
middlewares.push(thunkMiddleware);

const middlewareEnhancer = composeWithDevTools(applyMiddleware(...middlewares));
const enhancers = [middlewareEnhancer];
const composedEnhancers = compose(...enhancers);

const store: Store<ApplicationState> = createStore(
    rootReducer,
    //@ts-ignore
    composedEnhancers
);

export default store;
