import { user_register } from "../fixtures"
import mocked from "src/utils/mocked";

export const register_mock = (info: any) => {
    let conflict = info.email === user_register.email;

    return !conflict ?
        mocked(200, {
            data: info,
            message: 'enregistrement effectué avec success'
        }) : mocked(400, { message: 'un utilisateur à déjà cette addresse e-mail' })
}