import { register_mock } from "../mocks/AuthService.mock"

class AuthService {
    static register=(info:object) =>{
        return register_mock(info);
    }

}

export default AuthService;